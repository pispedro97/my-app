import { Button } from "../Button"
import './styles.css'

export function Container(){

    return(
            <div className="Container">
                
                <div className="CardContainer">

                    <div className="TitleContainer">
                        <p className="Title">Seja Bem-Vindo! :)</p>
                    </div>

                    <div className="InputsContainer">
                        <form>
                           <div className="formWrapper">
                                <div className="strongDiv">
                                        <strong className="inputStrong">
                                            Email:
                                        </strong>
                                </div>

                                <div className="inputDiv">
                                        <textarea
                                        className="Input"
                                        // onChange={()=> {}}
                                        // value = ""
                                        // required
                                        >
                                            
                                        </textarea>
                                </div>

                                <div className="strongDiv">
                                        <strong className="inputStrong">
                                            Senha:
                                        </strong>
                                </div>

                                <div className="inputDiv">
                                    <textarea
                                    className="Input"
                                    // onChange={()=> {}}
                                    // value = ""
                                    // required
                                    >
                                        
                                    </textarea>
                                </div>

                           </div>                           
                        </form>
                    </div>

                    <div className="ButtonContainer">
                        <Button />
                    </div>

                    <div className="FooterContainer">
                    
                    </div>
                </div>
             </div>
    )


}