import { Container } from './components/Container/index.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <Container />
    </div>
  );
}

export default App;
